/**
 * Tính lương nhân viên
 * 
 * input : work_day = n
 *  n = số ngày làm
 * 
 * todo :
 *  salary_1day = 100.000
 *  work_day * salary_1day
 * 
 * output : salary = n * 100.000
 * 
 */

var input = n;
var n = 1;
var salary_1day = 100000;
var salary = n * salary_1day;
console.log('salary: ', salary);

var n = 15;
var salary_1day = 100000;
var salary = n * salary_1day;
console.log('salary: ', salary);