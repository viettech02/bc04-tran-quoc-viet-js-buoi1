/**
 * Tính giá trị trung bình
 * 
 * Input : num1 -> num5
 * 
 * Todo : 
 * sum = num1 + num2 + num3 + num4 + num5 
 * sum / 5
 * output :
 * average = sum / 5
 */

var num1 = 2;
var num2 = 4;
var num3 = 5;
var num4 = 6;
var num5 = 8;
var sum = num1 + num2 + num3 + num4 + num5;
console.log('sum: ', sum);
var average = sum / 5;
console.log('average: ', average);



